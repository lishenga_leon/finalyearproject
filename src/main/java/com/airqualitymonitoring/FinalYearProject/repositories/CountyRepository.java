package com.airqualitymonitoring.FinalYearProject.repositories;

import com.airqualitymonitoring.FinalYearProject.model.County;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CountyRepository extends JpaRepository<County, Integer> {
    
}