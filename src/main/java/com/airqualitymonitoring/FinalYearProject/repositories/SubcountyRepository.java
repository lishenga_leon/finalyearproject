package com.airqualitymonitoring.FinalYearProject.repositories;

import com.airqualitymonitoring.FinalYearProject.model.County;
import com.airqualitymonitoring.FinalYearProject.model.Subcounty;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface SubcountyRepository extends JpaRepository<Subcounty, Integer> {
    
    Page<Subcounty> findByCounty(County county, Pageable pageable);
}