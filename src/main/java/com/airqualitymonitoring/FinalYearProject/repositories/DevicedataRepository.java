package com.airqualitymonitoring.FinalYearProject.repositories;

import com.airqualitymonitoring.FinalYearProject.model.Devicedata;
import com.airqualitymonitoring.FinalYearProject.model.Subcounty;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DevicedataRepository extends JpaRepository<Devicedata, Integer> {
    
    Page<Devicedata> findByDevice_id(Integer deviceId, Pageable pageable);

    Page<Devicedata> findByOrderByCreatedAtDesc(Pageable pageable);

    Page<Devicedata> findBySubcounty(Subcounty subcounty, Pageable pageable);

}