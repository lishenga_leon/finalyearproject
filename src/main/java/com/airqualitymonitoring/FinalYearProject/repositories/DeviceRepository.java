package com.airqualitymonitoring.FinalYearProject.repositories;

import java.util.List;
import java.util.Optional;

import com.airqualitymonitoring.FinalYearProject.model.Device;
import com.airqualitymonitoring.FinalYearProject.model.Subcounty;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DeviceRepository extends JpaRepository<Device, Integer> {

    Optional<Device> findBySerialNo(String serialNo);

    List <Device> findBySubcounty(Subcounty subcounty);

    Page<Device> findBySubcounty(Subcounty subcounty, Pageable pageable);
    
}