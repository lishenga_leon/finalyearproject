package com.airqualitymonitoring.FinalYearProject.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import com.airqualitymonitoring.FinalYearProject.requests.subcounty.RegisterSubcountyRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;



/**
 * Subcounty
 */
@Entity
@Table(name = "Subcounty")
public class Subcounty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "long")
    private Float lon;

    @Column(name = "latDelta")
    private Float latDelta;

    @Column(name = "lonDelta")
    private Float lonDelta;

    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore 
    private County county;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "subcounty_devicedata", joinColumns = @JoinColumn(name = "subcounty_id"), inverseJoinColumns = @JoinColumn(name = "devicedata_id"))
    private List<Devicedata> devicedata;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "subcounty_device", joinColumns = @JoinColumn(name = "subcounty_id"), inverseJoinColumns = @JoinColumn(name = "device_id"))
    private List<Device> device;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public Subcounty() {
    }

    public void registerSubcounty(RegisterSubcountyRequest registerSubcountyRequest) {
        this.name = registerSubcountyRequest.getName();
        this.lat = registerSubcountyRequest.getLat();
        this.lon = registerSubcountyRequest.getLon();
        this.lonDelta = registerSubcountyRequest.getLonDelta();
        this.latDelta = registerSubcountyRequest.getLatDelta();
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }

    public Subcounty(Subcounty subcounty) {
        this.id = subcounty.id;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLat() {
        return this.lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return this.lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLatDelta() {
        return this.latDelta;
    }

    public void setLatDelta(float latDelta) {
        this.latDelta = latDelta;
    }

    public float getLonDelta() {
        return this.lonDelta;
    }

    public void setLonDelta(float lonDelta) {
        this.lonDelta = lonDelta;
    }

    public County getCounty() {
        return this.county;
    }

    public void setCounty(County county) {
        this.county = county;
    }

    public List<Devicedata> getDevicedata() {
        return this.devicedata;
    }

    public void setDevicedata(List<Devicedata> devicedata) {
        this.devicedata = devicedata;
    }

    public List<Device> getDevice() {
        return this.device;
    }

    public void setDevice(List<Device> device) {
        this.device = device;
    }

    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", name='" + getName() + "'" +
            ", lat='" + getLat() + "'" +
            ", lon='" + getLon() + "'" +
            ", latDelta='" + getLatDelta() + "'" +
            ", lonDelta='" + getLonDelta() + "'" +
            ", county='" + getCounty() + "'" +
            ", devicedata='" + getDevicedata() + "'" +
            ", device='" + getDevice() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }


}