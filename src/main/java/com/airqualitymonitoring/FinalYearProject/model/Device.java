package com.airqualitymonitoring.FinalYearProject.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;

import com.airqualitymonitoring.FinalYearProject.requests.device.RegisterDeviceRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;



/**
 * Device
 */
@Entity
@Table(name = "device")
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "serial_no")
    private String serialNo;

    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore 
    private Subcounty subcounty;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "long")
    private Float lon;

    @Column(name = "latDelta")
    private Float latDelta;

    @Column(name = "lonDelta") 
    private Float lonDelta;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "device_devicedata", joinColumns = @JoinColumn(name = "device_id"), inverseJoinColumns = @JoinColumn(name = "devicedata_id"))
    private List<Devicedata> devicedata;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public void registerDevice(RegisterDeviceRequest registerDeviceRequest) {
        this.serialNo = registerDeviceRequest.getSerialNo();
        this.lat = registerDeviceRequest.getLat();
        this.lon = registerDeviceRequest.getLon();
        this.lonDelta = registerDeviceRequest.getLonDelta();
        this.latDelta = registerDeviceRequest.getLatDelta();
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }

    public Device() {
    }


    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSerialNo() {
        return this.serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Subcounty getSubcounty() {
        return this.subcounty;
    }

    public void setSubcounty(Subcounty subcounty) {
        this.subcounty = subcounty;
    }

    public float getLat() {
        return this.lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLon() {
        return this.lon;
    }

    public void setLon(float lon) {
        this.lon = lon;
    }

    public float getLatDelta() {
        return this.latDelta;
    }

    public void setLatDelta(float latDelta) {
        this.latDelta = latDelta;
    }

    public float getLonDelta() {
        return this.lonDelta;
    }

    public void setLonDelta(float lonDelta) {
        this.lonDelta = lonDelta;
    }

    public List<Devicedata> getDevicedata() {
        return this.devicedata;
    }

    public void setDevicedata(List<Devicedata> devicedata) {
        this.devicedata = devicedata;
    }

    public LocalDateTime getCreatedAt() {
        return this.createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return this.updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", serialNo='" + getSerialNo() + "'" +
            ", subcounty='" + getSubcounty() + "'" +
            ", lat='" + getLat() + "'" +
            ", lon='" + getLon() + "'" +
            ", latDelta='" + getLatDelta() + "'" +
            ", lonDelta='" + getLonDelta() + "'" +
            ", devicedata='" + getDevicedata() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
    
}