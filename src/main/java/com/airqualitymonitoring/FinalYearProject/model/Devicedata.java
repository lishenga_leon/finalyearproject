package com.airqualitymonitoring.FinalYearProject.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.airqualitymonitoring.FinalYearProject.requests.devicedata.LogDataRequest;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
/**
 * Devicedata
 */
@Entity
@Table(name = "Devicedata")
@Getter @Setter @ToString
public class Devicedata {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "aqi2.5")
    private Float aqi25;

    @Column(name = "aqi10")
    private Float aqi10;

    @Column(name = "mq_co")
    private Float mqSensorCO;

    @Column(name = "recorded_time")
    private LocalDateTime recordedTime;

    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore 
    private Device device;

    @ManyToOne(fetch=FetchType.LAZY)
    @JsonIgnore 
    private Subcounty subcounty;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;


    public void logData(LogDataRequest logDataRequest) {

        this.aqi10 = logDataRequest.aqi10;
        this.aqi25 = logDataRequest.aqi25;
        this.mqSensorCO = logDataRequest.mqSensorCO;
        this.recordedTime = logDataRequest.recordedTime;
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }

    public Devicedata() {
    }

    public Devicedata(Devicedata devicedata) {
        this.id = devicedata.id;
    }
}