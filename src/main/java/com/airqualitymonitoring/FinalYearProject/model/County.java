package com.airqualitymonitoring.FinalYearProject.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.airqualitymonitoring.FinalYearProject.requests.county.RegisterCountyRequest;

import javax.persistence.JoinColumn;



/**
 * County
 */
@Entity
@Table(name = "county")
public class County {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "lat")
    private Float lat;

    @Column(name = "long")
    private Float lon;

    @Column(name = "latDelta")
    private Float latDelta;

    @Column(name = "lonDelta")
    private Float lonDelta;

    @Column(name = "name")
    private String name;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "county_subcounty", joinColumns = @JoinColumn(name = "county_id"), inverseJoinColumns = @JoinColumn(name = "subcounty_id"))
    private List<Subcounty> subcounty;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    public County() {
    }

    public void registerCounty(RegisterCountyRequest registerCountyRequest) {
        this.name = registerCountyRequest.getName();
        this.lat = registerCountyRequest.getLat();
        this.lon = registerCountyRequest.getLon();
        this.lonDelta = registerCountyRequest.getLonDelta();
        this.latDelta = registerCountyRequest.getLatDelta();
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }

    public County(County county) {
        this.id = county.id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Subcounty> getSubcounty() {
        return subcounty;
    }

    public void setSubcounty(List<Subcounty> subcounty) {
        this.subcounty = subcounty;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }


    /**
     * @return float return the lat
     */
    public float getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(float lat) {
        this.lat = lat;
    }

    /**
     * @return float return the lon
     */
    public float getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(float lon) {
        this.lon = lon;
    }

    /**
     * @return float return the latDelta
     */
    public float getLatDelta() {
        return latDelta;
    }

    /**
     * @param latDelta the latDelta to set
     */
    public void setLatDelta(float latDelta) {
        this.latDelta = latDelta;
    }

    /**
     * @return float return the lonDelta
     */
    public float getLonDelta() {
        return lonDelta;
    }

    /**
     * @param lonDelta the lonDelta to set
     */
    public void setLonDelta(float lonDelta) {
        this.lonDelta = lonDelta;
    }

    @Override
    public String toString() {
        return "{" +
            " id='" + getId() + "'" +
            ", lat='" + getLat() + "'" +
            ", lon='" + getLon() + "'" +
            ", latDelta='" + getLatDelta() + "'" +
            ", lonDelta='" + getLonDelta() + "'" +
            ", name='" + getName() + "'" +
            ", subcounty='" + getSubcounty() + "'" +
            ", createdAt='" + getCreatedAt() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }

}