package com.airqualitymonitoring.FinalYearProject.controllers;

import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.Device;
import com.airqualitymonitoring.FinalYearProject.requests.device.DeleteDeviceRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.GetAllDevicesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.GetParticularDeviceRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.GetSubcountyDevicesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.RegisterDeviceRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.UpdateDeviceRequest;
import com.airqualitymonitoring.FinalYearProject.responses.device.DeleteDeviceResponse;
import com.airqualitymonitoring.FinalYearProject.responses.device.GetAllDevicesResponse;
import com.airqualitymonitoring.FinalYearProject.responses.device.GetParticularDeviceResponse;
import com.airqualitymonitoring.FinalYearProject.responses.device.GetSubcountyDevicesResponse;
import com.airqualitymonitoring.FinalYearProject.responses.device.RegisterDeviceResponse;
import com.airqualitymonitoring.FinalYearProject.responses.device.UpdateDeviceResponse;
import com.airqualitymonitoring.FinalYearProject.service.DeviceService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * DevicedataController
 */

@RestController
@RequestMapping("api/device")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    private RegisterDeviceResponse registerDeviceResponse = new RegisterDeviceResponse();

    private GetParticularDeviceResponse getParticularDeviceResponse = new GetParticularDeviceResponse();

    private GetSubcountyDevicesResponse getSubcountyDevicesResponse = new GetSubcountyDevicesResponse();

    private GetAllDevicesResponse getAllDevicesResponse = new GetAllDevicesResponse();

    private DeleteDeviceResponse deleteDeviceResponse = new DeleteDeviceResponse();

    private UpdateDeviceResponse updateDeviceResponse = new UpdateDeviceResponse();


    @RequestMapping(value = "registerDevice", method = RequestMethod.POST)
    public RegisterDeviceResponse registerDevice(@RequestBody RegisterDeviceRequest registerDeviceRequest) {

        Device device = deviceService.registerDevice(registerDeviceRequest);
        registerDeviceResponse.setData(device);
        registerDeviceResponse.setMessage("Success");
        registerDeviceResponse.setStatus(200);
        return registerDeviceResponse;
    }


    @RequestMapping(value = "getParticularDevice", method = RequestMethod.POST)
    public GetParticularDeviceResponse getParticularDevice(@RequestBody GetParticularDeviceRequest getParticularDeviceRequest) throws Exception {

        Device device = deviceService.getParticularDevice(getParticularDeviceRequest);
        getParticularDeviceResponse.setData(device);
        getParticularDeviceResponse.setMessage("Success");
        getParticularDeviceResponse.setStatus(200);
        return getParticularDeviceResponse;
    }


    @RequestMapping(value = "getAllDevices", method = RequestMethod.POST)
    public GetAllDevicesResponse getAllDevices(@RequestBody GetAllDevicesRequest getAllDevicesRequest) {

        List <Device> device = deviceService.getAllDevices(getAllDevicesRequest);
        getAllDevicesResponse.setData(device);
        getAllDevicesResponse.setMessage("Success");
        getAllDevicesResponse.setStatus(200);
        return getAllDevicesResponse;
    }

    @RequestMapping(value = "getSubcountyDevices", method = RequestMethod.POST)
    public GetSubcountyDevicesResponse getSubcountyDevices(@RequestBody GetSubcountyDevicesRequest getSubcountyDevicesRequest) throws Exception {

        List <Device> device = deviceService.getSubcountyDevices(getSubcountyDevicesRequest);
        getSubcountyDevicesResponse.setData(device);
        getSubcountyDevicesResponse.setMessage("Success");
        getSubcountyDevicesResponse.setStatus(200);
        return getSubcountyDevicesResponse;
    }

    @RequestMapping(value = "deleteDevice", method = RequestMethod.DELETE)
    public DeleteDeviceResponse deleteDevice(@RequestBody DeleteDeviceRequest deleteDeviceRequest) {

        deviceService.deleteDevice(deleteDeviceRequest);
        deleteDeviceResponse.setMessage("Success");
        deleteDeviceResponse.setStatus(200);
        return deleteDeviceResponse;
    }

    @RequestMapping(value = "updateDevice", method = RequestMethod.POST)
    public UpdateDeviceResponse updateDevice(@RequestBody UpdateDeviceRequest updateDeviceRequest) throws Exception {

        Device device = deviceService.updateDevice(updateDeviceRequest);
        updateDeviceResponse.setData(device);
        updateDeviceResponse.setMessage("Success");
        updateDeviceResponse.setStatus(200);
        return updateDeviceResponse;
    }
}