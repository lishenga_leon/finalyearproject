package com.airqualitymonitoring.FinalYearProject.controllers;

import java.time.LocalDateTime;
import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.Devicedata;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.GetAllDeviceDataRequest;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.GetParticularDeviceDataRequest;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.GetSubcountyDeviceDataRequest;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.LogDataRequest;
import com.airqualitymonitoring.FinalYearProject.responses.devicedata.GetAllDeviceDataResponse;
import com.airqualitymonitoring.FinalYearProject.responses.devicedata.GetParticularDeviceDataResponse;
import com.airqualitymonitoring.FinalYearProject.responses.devicedata.GetSubcountyDeviceDataResponse;
import com.airqualitymonitoring.FinalYearProject.responses.devicedata.LogDataResponse;
import com.airqualitymonitoring.FinalYearProject.service.DevicedataService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * DevicedataController
 */

@RestController
@RequestMapping("api/devicedata")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class DevicedataController {

    @Autowired
    private DevicedataService devicedataService;

    private LogDataResponse logDataResponse = new LogDataResponse();

    private GetParticularDeviceDataResponse getParticularDeviceDataResponse = new GetParticularDeviceDataResponse();

    private GetAllDeviceDataResponse getAllDeviceDataResponse = new GetAllDeviceDataResponse();

    private GetSubcountyDeviceDataResponse getSubcountyDeviceDataResponse = new GetSubcountyDeviceDataResponse();


    @RequestMapping(path="/logData/{serialNo}/{aqi25}/{mqSensorCO}/{aqi10}", method = RequestMethod.GET)
    public LogDataResponse logData(@PathVariable("serialNo") String serialNo, @PathVariable("aqi25") Float aqi25, @PathVariable("mqSensorCO") Float mqSensorCO, @PathVariable("aqi10") Float aqi10) {

        LogDataRequest logDataRequest = new LogDataRequest();
        logDataRequest.setMqSensorCO(mqSensorCO);
        logDataRequest.setSerialNo(serialNo);
        logDataRequest.setAqi10(aqi10);
        logDataRequest.setAqi25(aqi25);
        logDataRequest.setRecordedTime(LocalDateTime.now());

        Devicedata devicedata = devicedataService.logData(logDataRequest);
        logDataResponse.setData(devicedata);
        logDataResponse.setMessage("Success");
        logDataResponse.setStatus(200);
        return logDataResponse;
    }


    @RequestMapping(value = "getParticularDeviceData", method = RequestMethod.POST)
    public GetParticularDeviceDataResponse getParticularDeviceData(@RequestBody GetParticularDeviceDataRequest getParticularDeviceDataRequest) {

        List <Devicedata> devicedata = devicedataService.getParticularDeviceData(getParticularDeviceDataRequest);
        getParticularDeviceDataResponse.setData(devicedata);
        getParticularDeviceDataResponse.setMessage("Success");
        getParticularDeviceDataResponse.setStatus(200);
        return getParticularDeviceDataResponse;
    }


    @RequestMapping(value = "getAllDeviceData", method = RequestMethod.POST)
    public GetAllDeviceDataResponse getAllDeviceData(@RequestBody GetAllDeviceDataRequest getAllDeviceDataRequest) {

        List <Devicedata> devicedata = devicedataService.getAllDeviceData(getAllDeviceDataRequest);
        getAllDeviceDataResponse.setData(devicedata);
        getAllDeviceDataResponse.setMessage("Success");
        getAllDeviceDataResponse.setStatus(200);
        return getAllDeviceDataResponse;
    }

    @RequestMapping(value = "getSubcountyDeviceData", method = RequestMethod.POST)
    public GetSubcountyDeviceDataResponse getSubcountyDeviceData(@RequestBody GetSubcountyDeviceDataRequest getSubcountyDeviceDataRequest) throws Exception {

        List <Devicedata> devicedata = devicedataService.getSubcountyDeviceData(getSubcountyDeviceDataRequest);
        getSubcountyDeviceDataResponse.setData(devicedata);
        getSubcountyDeviceDataResponse.setMessage("Success");
        getSubcountyDeviceDataResponse.setStatus(200);
        return getSubcountyDeviceDataResponse;
    }
}