package com.airqualitymonitoring.FinalYearProject.controllers;

import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.County;
import com.airqualitymonitoring.FinalYearProject.requests.county.DeleteCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.GetAllCountiesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.GetParticularCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.RegisterCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.UpdateCountyRequest;
import com.airqualitymonitoring.FinalYearProject.responses.county.DeleteCountyResponse;
import com.airqualitymonitoring.FinalYearProject.responses.county.GetAllCountiesResponse;
import com.airqualitymonitoring.FinalYearProject.responses.county.GetParticularCountyResponse;
import com.airqualitymonitoring.FinalYearProject.responses.county.RegisterCountyResponse;
import com.airqualitymonitoring.FinalYearProject.responses.county.UpdateCountyResponse;
import com.airqualitymonitoring.FinalYearProject.service.CountyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;



@RestController
@RequestMapping("api/county")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CountyController {

    @Autowired
    private CountyService countyService;

    private RegisterCountyResponse registerCountyResponse = new RegisterCountyResponse();

    private GetParticularCountyResponse getParticularCountyResponse = new GetParticularCountyResponse();

    private GetAllCountiesResponse getAllCountiesResponse = new GetAllCountiesResponse();

    private DeleteCountyResponse deleteCountyResponse = new DeleteCountyResponse();

    private UpdateCountyResponse updateCountyResponse = new UpdateCountyResponse();


    @RequestMapping(value = "registerCounty", method = RequestMethod.POST)
    public RegisterCountyResponse registerCounty(@RequestBody RegisterCountyRequest registerCountyRequest) {

        County county = countyService.registerCounty(registerCountyRequest);
        registerCountyResponse.setData(county);
        registerCountyResponse.setMessage("Success");
        registerCountyResponse.setStatus(200);
        return registerCountyResponse;
    }


    @RequestMapping(value = "getParticularCounty", method = RequestMethod.POST)
    public GetParticularCountyResponse getParticularCounty(@RequestBody GetParticularCountyRequest getParticularCountyRequest) throws Exception {

        County county = countyService.getParticularCounty(getParticularCountyRequest);
        getParticularCountyResponse.setData(county);
        getParticularCountyResponse.setMessage("Success");
        getParticularCountyResponse.setStatus(200);
        return getParticularCountyResponse;
    }


    @RequestMapping(value = "getAllCounties", method = RequestMethod.POST)
    public GetAllCountiesResponse getAllCounties(@RequestBody GetAllCountiesRequest getAllCountiesRequest) {

        List <County> county = countyService.getAllCounties(getAllCountiesRequest);
        getAllCountiesResponse.setData(county);
        getAllCountiesResponse.setMessage("Success");
        getAllCountiesResponse.setStatus(200);
        return getAllCountiesResponse;
    }

    @RequestMapping(value = "deleteCounty", method = RequestMethod.POST)
    public DeleteCountyResponse deleteCounty(@RequestBody DeleteCountyRequest deleteCountyRequest) {

        countyService.deleteCounty(deleteCountyRequest);
        deleteCountyResponse.setMessage("Success");
        deleteCountyResponse.setStatus(200);
        return deleteCountyResponse;
    }

    @RequestMapping(value = "updateCounty", method = RequestMethod.POST)
    public UpdateCountyResponse updateCounty(@RequestBody UpdateCountyRequest updateCountyRequest) throws Exception {

        County county = countyService.updateCounty(updateCountyRequest);
        updateCountyResponse.setData(county);
        updateCountyResponse.setMessage("Success");
        updateCountyResponse.setStatus(200);
        return updateCountyResponse;
    }
}