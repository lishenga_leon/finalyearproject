package com.airqualitymonitoring.FinalYearProject.controllers;

import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.Subcounty;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.DeleteSubcountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.GetAllSubcountiesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.GetParticularSubcountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.GetSubcountieCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.RegisterSubcountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.UpdateSubcountyRequest;
import com.airqualitymonitoring.FinalYearProject.responses.subcounty.DeleteSubcountyResponse;
import com.airqualitymonitoring.FinalYearProject.responses.subcounty.GetAllSubcountiesResponse;
import com.airqualitymonitoring.FinalYearProject.responses.subcounty.GetParticularSubcountyResponse;
import com.airqualitymonitoring.FinalYearProject.responses.subcounty.GetSubcountieCountyResponse;
import com.airqualitymonitoring.FinalYearProject.responses.subcounty.RegisterSubcountyResponse;
import com.airqualitymonitoring.FinalYearProject.responses.subcounty.UpdateSubcountyResponse;
import com.airqualitymonitoring.FinalYearProject.service.SubcountyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("api/Subcounty")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class SubcountyController {

    @Autowired
    private SubcountyService subcountyService;

    private RegisterSubcountyResponse registerSubcountyResponse = new RegisterSubcountyResponse();

    private GetParticularSubcountyResponse getParticularSubcountyResponse = new GetParticularSubcountyResponse();

    private GetSubcountieCountyResponse getSubcountieCountyResponse = new GetSubcountieCountyResponse();

    private GetAllSubcountiesResponse getAllSubcountiesResponse = new GetAllSubcountiesResponse();

    private DeleteSubcountyResponse deleteSubcountyResponse = new DeleteSubcountyResponse();

    private UpdateSubcountyResponse updateSubcountyResponse = new UpdateSubcountyResponse();


    @RequestMapping(value = "registerSubcounty", method = RequestMethod.POST)
    public RegisterSubcountyResponse registerSubcounty(@RequestBody RegisterSubcountyRequest registerSubcountyRequest) throws Exception {

        Subcounty subcounty = subcountyService.registerSubcounty(registerSubcountyRequest);
        registerSubcountyResponse.setData(subcounty);
        registerSubcountyResponse.setMessage("Success");
        registerSubcountyResponse.setStatus(200);
        return registerSubcountyResponse;
    }


    @RequestMapping(value = "getParticularSubcounty", method = RequestMethod.POST)
    public GetParticularSubcountyResponse getParticularSubcounty(@RequestBody GetParticularSubcountyRequest getParticularSubcountyRequest) throws Exception {

        Subcounty subcounty = subcountyService.getParticularSubcounty(getParticularSubcountyRequest);
        getParticularSubcountyResponse.setData(subcounty);
        getParticularSubcountyResponse.setMessage("Success");
        getParticularSubcountyResponse.setStatus(200);
        return getParticularSubcountyResponse;
    }

    @RequestMapping(value = "getSubcountiesCounty", method = RequestMethod.POST)
    public GetSubcountieCountyResponse getSubcountieCounty(@RequestBody GetSubcountieCountyRequest getSubcountieCountyRequest) throws Exception {

        List <Subcounty> subcounty = subcountyService.getSubcountieCounty(getSubcountieCountyRequest);
        getSubcountieCountyResponse.setData(subcounty);
        getSubcountieCountyResponse.setMessage("Success");
        getSubcountieCountyResponse.setStatus(200);
        return getSubcountieCountyResponse;
    }


    @RequestMapping(value = "getAllSubcounties", method = RequestMethod.POST)
    public GetAllSubcountiesResponse getAllSubcounties(@RequestBody GetAllSubcountiesRequest getAllSubcountiesRequest) {

        List <Subcounty> subcounty = subcountyService.getAllSubcounties(getAllSubcountiesRequest);
        getAllSubcountiesResponse.setData(subcounty);
        getAllSubcountiesResponse.setMessage("Success");
        getAllSubcountiesResponse.setStatus(200);
        return getAllSubcountiesResponse;
    }

    @RequestMapping(value = "deleteSubcounty", method = RequestMethod.POST)
    public DeleteSubcountyResponse deleteSubcounty(@RequestBody DeleteSubcountyRequest deleteSubcountyRequest) {

        subcountyService.deleteSubcounty(deleteSubcountyRequest);
        deleteSubcountyResponse.setMessage("Success");
        deleteSubcountyResponse.setStatus(200);
        return deleteSubcountyResponse;
    }

    @RequestMapping(value = "updateSubcounty", method = RequestMethod.POST)
    public UpdateSubcountyResponse updateSubcounty(@RequestBody UpdateSubcountyRequest updateSubcountyRequest) throws Exception {

        Subcounty subcounty = subcountyService.updateSubcounty(updateSubcountyRequest);
        updateSubcountyResponse.setData(subcounty);
        updateSubcountyResponse.setMessage("Success");
        updateSubcountyResponse.setStatus(200);
        return updateSubcountyResponse;
    }
}