package com.airqualitymonitoring.FinalYearProject.responses.devicedata;

import com.airqualitymonitoring.FinalYearProject.model.Devicedata;

/**
 * LogDataResponse
 */
public class LogDataResponse {

    private int status;

    private String message;

    private Devicedata data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Devicedata getData() {
        return data;
    }

    public void setData(Devicedata data) {
        this.data = data;
    }

}
