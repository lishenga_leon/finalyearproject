package com.airqualitymonitoring.FinalYearProject.responses.devicedata;

import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.Devicedata;


public class GetAllDeviceDataResponse {

    private int status;

    private String message;

    private List<Devicedata> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Devicedata> getData() {
        return data;
    }

    public void setData(List<Devicedata> devicedata) {
        this.data = devicedata;
    }

}