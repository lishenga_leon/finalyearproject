package com.airqualitymonitoring.FinalYearProject.responses.county;

import com.airqualitymonitoring.FinalYearProject.model.County;

public class GetParticularCountyResponse {

    private int status;

    private String message;

    private County data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public County getData() {
        return data;
    }

    public void setData(County county) {
        this.data = county;
    }
}