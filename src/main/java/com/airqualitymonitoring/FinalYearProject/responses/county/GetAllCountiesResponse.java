package com.airqualitymonitoring.FinalYearProject.responses.county;

import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.County;

public class GetAllCountiesResponse {

    private int status;

    private String message;

    private List<County> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<County> getData() {
        return data;
    }

    public void setData(List<County> county) {
        this.data = county;
    }
}