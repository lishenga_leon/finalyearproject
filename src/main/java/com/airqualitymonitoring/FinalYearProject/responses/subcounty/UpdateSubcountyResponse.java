package com.airqualitymonitoring.FinalYearProject.responses.subcounty;

import com.airqualitymonitoring.FinalYearProject.model.Subcounty;

public class UpdateSubcountyResponse {
    
    private int status;

    private String message;

    private Subcounty data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Subcounty getData() {
        return data;
    }

    public void setData(Subcounty data) {
        this.data = data;
    }
}