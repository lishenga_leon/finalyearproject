package com.airqualitymonitoring.FinalYearProject.responses.subcounty;

import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.Subcounty;

public class GetSubcountieCountyResponse {
    
    private int status;

    private String message;

    private List<Subcounty> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Subcounty> getData() {
        return data;
    }

    public void setData(List<Subcounty> subcounty) {
        this.data = subcounty;
    }
}