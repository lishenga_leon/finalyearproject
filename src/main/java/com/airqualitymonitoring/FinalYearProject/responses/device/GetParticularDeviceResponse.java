package com.airqualitymonitoring.FinalYearProject.responses.device;

import com.airqualitymonitoring.FinalYearProject.model.Device;

public class GetParticularDeviceResponse {

    private int status;

    private String message;

    private Device data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Device getData() {
        return data;
    }

    public void setData(Device device) {
        this.data = device;
    }
}