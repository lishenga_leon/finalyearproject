package com.airqualitymonitoring.FinalYearProject.responses.device;

import java.util.List;

import com.airqualitymonitoring.FinalYearProject.model.Device;

public class GetAllDevicesResponse {

    private int status;

    private String message;

    private List<Device> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Device> getData() {
        return data;
    }

    public void setData(List<Device> device) {
        this.data = device;
    }
}