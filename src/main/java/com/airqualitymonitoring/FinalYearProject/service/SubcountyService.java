package com.airqualitymonitoring.FinalYearProject.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.airqualitymonitoring.FinalYearProject.exceptions.CountyExceptions;
import com.airqualitymonitoring.FinalYearProject.exceptions.SubcountyExceptions;
import com.airqualitymonitoring.FinalYearProject.model.County;
import com.airqualitymonitoring.FinalYearProject.model.Subcounty;
import com.airqualitymonitoring.FinalYearProject.repositories.CountyRepository;
import com.airqualitymonitoring.FinalYearProject.repositories.SubcountyRepository;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.DeleteSubcountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.GetAllSubcountiesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.GetParticularSubcountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.GetSubcountieCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.RegisterSubcountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.subcounty.UpdateSubcountyRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class SubcountyService {

    @Autowired
    private SubcountyRepository subcountyRepository;

    @Autowired
    private CountyRepository countyRepository;

    public Subcounty registerSubcounty(RegisterSubcountyRequest registerSubcountyRequest) throws Exception {

        Optional<County> county = countyRepository.findById(registerSubcountyRequest.getCounty());

        if (!county.isPresent()) {
            throw new SubcountyExceptions(401, "County not Found");
        }

        Subcounty subcounty = new Subcounty();
        subcounty.registerSubcounty(registerSubcountyRequest);
        subcounty.setCounty(county.get());
        subcountyRepository.save(subcounty);

        return subcounty;
    }

    public Subcounty getParticularSubcounty(GetParticularSubcountyRequest getParticularSubcountyRequest) throws Exception {

        Optional<Subcounty> part = subcountyRepository.findById(getParticularSubcountyRequest.subcountyId);

        if (!part.isPresent()) {
            throw new SubcountyExceptions(401, "Subcounty not Found");
        }

        Subcounty subcounty = part.get();
        return subcounty;
    }

    public List<Subcounty> getSubcountieCounty(GetSubcountieCountyRequest getSubcountieCountyRequest) throws Exception {

        Optional<County> part = countyRepository.findById(getSubcountieCountyRequest.countyId);

        if (!part.isPresent()) {
            throw new SubcountyExceptions(401, "County not Found");
        }

        Pageable find = PageRequest.of(getSubcountieCountyRequest.getPage(), getSubcountieCountyRequest.getItems());
        Page<Subcounty> subcounty = subcountyRepository.findByCounty(part.get(), find);

        return subcounty.getContent();
    }

    public List<Subcounty> getAllSubcounties(GetAllSubcountiesRequest getAllSubcountiesRequest) {
        Pageable find = PageRequest.of(getAllSubcountiesRequest.getPage(), getAllSubcountiesRequest.getItems());
        Page<Subcounty> subcounty = subcountyRepository.findAll(find);

        return subcounty.getContent();
    }

    public void deleteSubcounty(DeleteSubcountyRequest deleteSubcountyRequest) {

        subcountyRepository.deleteById(deleteSubcountyRequest.subcountyId);
    }

    public Subcounty updateSubcounty(UpdateSubcountyRequest updateSubcountyRequest) throws Exception {
        /**
         * Encrypt Passwords
         */
        Optional<Subcounty> part = subcountyRepository.findById(updateSubcountyRequest.getSubcountyId());
        Optional<County> parts = countyRepository.findById(updateSubcountyRequest.getCountyId());

        if (!parts.isPresent()) {
            throw new CountyExceptions(401, "County not Found");
        }

        if (!part.isPresent()) {
            throw new SubcountyExceptions(401, "Subcounty not Found");
        }

        Subcounty subcounty = part.get();

        if (updateSubcountyRequest.getName() != null) {
            subcounty.setName(updateSubcountyRequest.getName());
        }

        if (updateSubcountyRequest.getCountyId() != null) {
            subcounty.setCounty(parts.get());
        }

        if (updateSubcountyRequest.getLat() != 0) {
            subcounty.setLat(updateSubcountyRequest.getLat());
        }

        if (updateSubcountyRequest.getLon() != 0) {
            subcounty.setLon(updateSubcountyRequest.getLon());
        }

        if (updateSubcountyRequest.getLatDelta() != 0) {
            subcounty.setLatDelta(updateSubcountyRequest.getLatDelta());
        }

        if (updateSubcountyRequest.getLonDelta() != 0) {
            subcounty.setLonDelta(updateSubcountyRequest.getLonDelta());
        }

        subcounty.setUpdatedAt(LocalDateTime.now());

        subcountyRepository.save(subcounty);

        return subcounty;
    }
}