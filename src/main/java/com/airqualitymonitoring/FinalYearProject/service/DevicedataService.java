package com.airqualitymonitoring.FinalYearProject.service;

import java.util.List;
import java.util.Optional;

import com.airqualitymonitoring.FinalYearProject.exceptions.DeviceExceptions;
import com.airqualitymonitoring.FinalYearProject.model.Device;
import com.airqualitymonitoring.FinalYearProject.model.Devicedata;
import com.airqualitymonitoring.FinalYearProject.model.Subcounty;
import com.airqualitymonitoring.FinalYearProject.repositories.DeviceRepository;
import com.airqualitymonitoring.FinalYearProject.repositories.DevicedataRepository;
import com.airqualitymonitoring.FinalYearProject.repositories.SubcountyRepository;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.GetAllDeviceDataRequest;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.GetParticularDeviceDataRequest;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.GetSubcountyDeviceDataRequest;
import com.airqualitymonitoring.FinalYearProject.requests.devicedata.LogDataRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * DevicedataService
 */
@Service
public class DevicedataService {

    @Autowired
    private DevicedataRepository devicedataRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private SubcountyRepository subcountyRepository;

    public Devicedata logData(LogDataRequest logDataRequest) {

        Optional<Device> device = deviceRepository.findBySerialNo(logDataRequest.serialNo);

        Devicedata devicedata = new Devicedata();
        devicedata.logData(logDataRequest);
        devicedata.setDevice(device.get());
        devicedata.setSubcounty(device.get().getSubcounty());
        devicedataRepository.save(devicedata);

        return devicedata;
    }

    public List<Devicedata> getParticularDeviceData(GetParticularDeviceDataRequest getParticularDeviceDataRequest) {
        Pageable find = PageRequest.of(getParticularDeviceDataRequest.page, getParticularDeviceDataRequest.items);
        Page<Devicedata> devicedata = devicedataRepository.findByDevice_id(getParticularDeviceDataRequest.deviceId, find);

        return devicedata.getContent();
    }

    public List<Devicedata> getAllDeviceData(GetAllDeviceDataRequest getAllDeviceDataRequest) {
        Pageable find = PageRequest.of(getAllDeviceDataRequest.page, getAllDeviceDataRequest.items);
        Page<Devicedata> devicedata = devicedataRepository.findAll(find);

        return devicedata.getContent();
    }

    public List<Devicedata> getSubcountyDeviceData(GetSubcountyDeviceDataRequest getSubcountyDeviceDataRequest)
            throws Exception {

        Optional<Subcounty> part = subcountyRepository.findById(getSubcountyDeviceDataRequest.subcountyId);

        if (!part.isPresent()) {
            throw new DeviceExceptions(401, "Subcounty not Found");
        }

        Pageable find = PageRequest.of(getSubcountyDeviceDataRequest.page, getSubcountyDeviceDataRequest.items);
        Page<Devicedata> devicedata = devicedataRepository.findBySubcounty(part.get(), find);

        return devicedata.getContent();
    }
}