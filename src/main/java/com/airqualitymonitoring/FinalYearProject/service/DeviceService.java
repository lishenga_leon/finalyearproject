package com.airqualitymonitoring.FinalYearProject.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.airqualitymonitoring.FinalYearProject.exceptions.DeviceExceptions;
import com.airqualitymonitoring.FinalYearProject.model.Device;
import com.airqualitymonitoring.FinalYearProject.model.Subcounty;
import com.airqualitymonitoring.FinalYearProject.repositories.DeviceRepository;
import com.airqualitymonitoring.FinalYearProject.repositories.SubcountyRepository;
import com.airqualitymonitoring.FinalYearProject.requests.device.DeleteDeviceRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.GetAllDevicesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.GetParticularDeviceRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.GetSubcountyDevicesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.RegisterDeviceRequest;
import com.airqualitymonitoring.FinalYearProject.requests.device.UpdateDeviceRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@Service
public class DeviceService {

    @Autowired
    private SubcountyRepository subcountyRepository;

    @Autowired
    private DeviceRepository deviceRepository;


    public Device registerDevice(RegisterDeviceRequest registerDeviceRequest) {

        Optional <Subcounty> subcounty = subcountyRepository.findById(registerDeviceRequest.getSubcounty());

        Device device = new Device();
        device.registerDevice(registerDeviceRequest);
        device.setSubcounty(subcounty.get());
        deviceRepository.save(device);
        
        return device;
    }

    public List <Device> getSubcountyDevices(GetSubcountyDevicesRequest getSubcountyDevicesRequest) throws Exception {

        Optional<Subcounty> part = subcountyRepository.findById(getSubcountyDevicesRequest.getSubcountyId());

        if (!part.isPresent()) {
            throw new DeviceExceptions(401, "Subcounty not Found");
        }

        Pageable find = PageRequest.of(getSubcountyDevicesRequest.getPage(), getSubcountyDevicesRequest.getItems());
        Page<Device> device = deviceRepository.findBySubcounty(part.get(), find);

        return device.getContent();
    }

    public Device getParticularDevice(GetParticularDeviceRequest getParticularDeviceRequest) throws Exception {

        Optional <Device> part = deviceRepository.findById(getParticularDeviceRequest.deviceId);

        if(!part.isPresent()){
            throw new DeviceExceptions(401, "Device not Found");
        }
        
        Device device = part.get();
        return device;
    }

    public List <Device> getAllDevices(GetAllDevicesRequest getAllDevicesRequest) {
        Pageable find = PageRequest.of(getAllDevicesRequest.page, getAllDevicesRequest.items);
        Page <Device> device = deviceRepository.findAll(find);

        return device.getContent();
    }

    public void deleteDevice(DeleteDeviceRequest deleteDeviceRequest) {

        deviceRepository.deleteById(deleteDeviceRequest.deviceId);
    }

    public Device updateDevice(UpdateDeviceRequest updateDeviceRequest) throws Exception {
        /**
         * Encrypt Passwords
         */
        Optional <Device> part = deviceRepository.findById(updateDeviceRequest.getDeviceId());

        Optional <Subcounty> sub = subcountyRepository.findById(updateDeviceRequest.getSubcounty());

        if(!part.isPresent()){
            throw new DeviceExceptions(401, "Device not Found");
        }

        if(!sub.isPresent()){
            throw new DeviceExceptions(401, "Subcounty not Found");
        }

        Subcounty subcounty = sub.get();

        Device device = part.get();

        if(updateDeviceRequest.getSerialNo() != null){
            device.setSerialNo(updateDeviceRequest.getSerialNo());
        }

        if(updateDeviceRequest.getSubcounty() != null){
            device.setSubcounty(subcounty);
        }

        if(updateDeviceRequest.getLat() != 0){
            device.setLat(updateDeviceRequest.getLat());
        }

        if(updateDeviceRequest.getLon() != 0){
            device.setLon(updateDeviceRequest.getLon());
        }

        if(updateDeviceRequest.getLatDelta() != 0){
            device.setLatDelta(updateDeviceRequest.getLatDelta());
        }

        if(updateDeviceRequest.getLonDelta() != 0){
            device.setLonDelta(updateDeviceRequest.getLonDelta());
        }

        device.setUpdatedAt(LocalDateTime.now());

        deviceRepository.save(device);
        
        return device;
    }
}