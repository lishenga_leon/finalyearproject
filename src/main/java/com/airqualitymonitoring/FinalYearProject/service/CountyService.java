package com.airqualitymonitoring.FinalYearProject.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import com.airqualitymonitoring.FinalYearProject.exceptions.CountyExceptions;
import com.airqualitymonitoring.FinalYearProject.model.County;
import com.airqualitymonitoring.FinalYearProject.repositories.CountyRepository;
import com.airqualitymonitoring.FinalYearProject.requests.county.DeleteCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.GetAllCountiesRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.GetParticularCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.RegisterCountyRequest;
import com.airqualitymonitoring.FinalYearProject.requests.county.UpdateCountyRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CountyService {

    @Autowired
    private CountyRepository countyRepository;


    public County registerCounty(RegisterCountyRequest registerCountyRequest) {

        County county = new County();
        county.registerCounty(registerCountyRequest);
        countyRepository.save(county);
        
        return county;
    }

    public County getParticularCounty(GetParticularCountyRequest getParticularCountyRequest) throws Exception {

        Optional <County> part = countyRepository.findById(getParticularCountyRequest.countyId);

        if(!part.isPresent()){
            throw new CountyExceptions(401, "County not Found");
        }
        
        County county = part.get();
        return county;
    }

    public List <County> getAllCounties(GetAllCountiesRequest getAllCountiesRequest) {
        Pageable find = PageRequest.of(getAllCountiesRequest.page, getAllCountiesRequest.items);
        Page <County> county = countyRepository.findAll(find);

        return county.getContent();
    }

    public void deleteCounty(DeleteCountyRequest deleteCountyRequest) {

        countyRepository.deleteById(deleteCountyRequest.countyId);
    }

    public County updateCounty(UpdateCountyRequest updateCountyRequest) throws Exception {
        /**
         * Encrypt Passwords
         */
        Optional <County> part = countyRepository.findById(updateCountyRequest.getCountyId());

        if(!part.isPresent()){
            throw new CountyExceptions(401, "County not Found");
        }

        County county = part.get();

        if(updateCountyRequest.getName() != null){
            county.setName(updateCountyRequest.getName());
        }

        if(updateCountyRequest.getLat() != 0){
            county.setLat(updateCountyRequest.getLat());
        }

        if(updateCountyRequest.getLon() != 0){
            county.setLon(updateCountyRequest.getLon());
        }

        if(updateCountyRequest.getLatDelta() != 0){
            county.setLatDelta(updateCountyRequest.getLatDelta());
        }

        if(updateCountyRequest.getLonDelta() != 0){
            county.setLonDelta(updateCountyRequest.getLonDelta());
        }

        county.setUpdatedAt(LocalDateTime.now());

        countyRepository.save(county);
        
        return county;
    }
}