package com.airqualitymonitoring.FinalYearProject.requests.device;

import javax.validation.constraints.NotNull;

public class UpdateDeviceRequest {

    @NotNull(message = "DeviceID must be provided")
    private Integer deviceId;

    @NotNull(message = "Serial NO must be provided")
    private String serialNo;

    @NotNull(message = "subcounty must be provided")
    private Integer subcounty;

    private float lat;

    private float lon;

    private float latDelta;

    private float lonDelta;

    public String getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String serialNo) {
        this.serialNo = serialNo;
    }

    public Integer getSubcounty() {
        return subcounty;
    }

    public void setSubcounty(Integer subcounty) {
        this.subcounty = subcounty;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return float return the lat
     */
    public float getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(float lat) {
        this.lat = lat;
    }

    /**
     * @return float return the lon
     */
    public float getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(float lon) {
        this.lon = lon;
    }

    /**
     * @return float return the latDelta
     */
    public float getLatDelta() {
        return latDelta;
    }

    /**
     * @param latDelta the latDelta to set
     */
    public void setLatDelta(float latDelta) {
        this.latDelta = latDelta;
    }

    /**
     * @return float return the lonDelta
     */
    public float getLonDelta() {
        return lonDelta;
    }

    /**
     * @param lonDelta the lonDelta to set
     */
    public void setLonDelta(float lonDelta) {
        this.lonDelta = lonDelta;
    }

}