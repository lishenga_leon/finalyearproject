package com.airqualitymonitoring.FinalYearProject.requests.device;

import javax.validation.constraints.NotNull;

public class GetSubcountyDevicesRequest {
    
    public int page;

    public int items;

    @NotNull(message = "SubcountyID must be provided")
    private int subcountyId;

    public int getSubcountyId() {
        return subcountyId;
    }

    public void setSubcountyId(int subcountyId) {
        this.subcountyId = subcountyId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }
}