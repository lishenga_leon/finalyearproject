package com.airqualitymonitoring.FinalYearProject.requests.device;

import javax.validation.constraints.NotNull;

public class DeleteDeviceRequest {

    @NotNull(message = "DeviceID must be provided")
    public Integer deviceId;

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }
}