package com.airqualitymonitoring.FinalYearProject.requests.devicedata;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
/**
 * LogDataRequest
 */

@Getter @Setter
public class LogDataRequest {

    @NotNull(message = "Serial NO must be provided")
    public String serialNo;

    @NotNull(message = "aqi25 must be provided")
    public Float aqi25;

    @NotNull(message = "mqSensorCO must be provided")
    public Float mqSensorCO;

    @NotNull(message = "aqi10 must be provided")
    public Float aqi10;

    @NotNull(message = "Recorded Time must be provided")
    public LocalDateTime recordedTime;

}