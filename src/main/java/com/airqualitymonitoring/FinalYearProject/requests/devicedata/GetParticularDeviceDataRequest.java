package com.airqualitymonitoring.FinalYearProject.requests.devicedata;

import javax.validation.constraints.NotNull;

/**
 * GetParticularDeviceDataRequest
 */
public class GetParticularDeviceDataRequest {


    @NotNull(message = "DeviceID must be provided")
    public Integer deviceId;

    public int page;

    public int items;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

}