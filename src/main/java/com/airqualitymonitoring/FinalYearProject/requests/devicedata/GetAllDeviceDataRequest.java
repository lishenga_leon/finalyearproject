package com.airqualitymonitoring.FinalYearProject.requests.devicedata;

public class GetAllDeviceDataRequest{

    
    public int page;

    public int items;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

}