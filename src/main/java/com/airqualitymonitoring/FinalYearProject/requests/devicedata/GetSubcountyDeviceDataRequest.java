package com.airqualitymonitoring.FinalYearProject.requests.devicedata;

import javax.validation.constraints.NotNull;

/**
 * GetSubcountyDeviceDataRequest
 */
public class GetSubcountyDeviceDataRequest {


    @NotNull(message = "SubcountyID must be provided")
    public Integer subcountyId;

    @NotNull(message = "Page must be provided")
    public int page;

    @NotNull(message = "Items must be provided")
    public int items;

    public Integer getSubcountyId() {
        return subcountyId;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public void setSubcountyId(Integer subcountyId) {
        this.subcountyId = subcountyId;
    }

}