package com.airqualitymonitoring.FinalYearProject.requests.subcounty;

import javax.validation.constraints.NotNull;

public class UpdateSubcountyRequest {
    
    @NotNull(message = "SubCountyID must be provided")
    private Integer subcountyId;

    @NotNull(message = "CountyID must be provided")
    private Integer countyId;

    @NotNull(message = "Name must be provided")
    private String name;

    private float lat;

    private float lon;

    private float latDelta;

    private float lonDelta;

    public Integer getSubcountyId() {
        return subcountyId;
    }

    public void setSubcountyId(Integer subcountyId) {
        this.subcountyId = subcountyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }

    /**
     * @return float return the lat
     */
    public float getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(float lat) {
        this.lat = lat;
    }

    /**
     * @return float return the lon
     */
    public float getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(float lon) {
        this.lon = lon;
    }

    /**
     * @return float return the latDelta
     */
    public float getLatDelta() {
        return latDelta;
    }

    /**
     * @param latDelta the latDelta to set
     */
    public void setLatDelta(float latDelta) {
        this.latDelta = latDelta;
    }

    /**
     * @return float return the lonDelta
     */
    public float getLonDelta() {
        return lonDelta;
    }

    /**
     * @param lonDelta the lonDelta to set
     */
    public void setLonDelta(float lonDelta) {
        this.lonDelta = lonDelta;
    }

}