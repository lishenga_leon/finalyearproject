package com.airqualitymonitoring.FinalYearProject.requests.subcounty;

import javax.validation.constraints.NotNull;

public class RegisterSubcountyRequest {
    
    @NotNull(message = "Name must be provided")
    private String name;

    @NotNull(message = "County must be provided")
    private int county;

    private float lat;

    private float lon;

    private float latDelta;

    private float lonDelta;

    public String getName() {
        return name;
    }

    public int getCounty() {
        return county;
    }

    public void setCounty(int county) {
        this.county = county;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return float return the lat
     */
    public float getLat() {
        return lat;
    }

    /**
     * @param lat the lat to set
     */
    public void setLat(float lat) {
        this.lat = lat;
    }

    /**
     * @return float return the lon
     */
    public float getLon() {
        return lon;
    }

    /**
     * @param lon the lon to set
     */
    public void setLon(float lon) {
        this.lon = lon;
    }

    /**
     * @return float return the latDelta
     */
    public float getLatDelta() {
        return latDelta;
    }

    /**
     * @param latDelta the latDelta to set
     */
    public void setLatDelta(float latDelta) {
        this.latDelta = latDelta;
    }

    /**
     * @return float return the lonDelta
     */
    public float getLonDelta() {
        return lonDelta;
    }

    /**
     * @param lonDelta the lonDelta to set
     */
    public void setLonDelta(float lonDelta) {
        this.lonDelta = lonDelta;
    }

}