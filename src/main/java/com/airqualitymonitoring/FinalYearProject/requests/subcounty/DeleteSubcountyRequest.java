package com.airqualitymonitoring.FinalYearProject.requests.subcounty;

import javax.validation.constraints.NotNull;

public class DeleteSubcountyRequest {
    
    @NotNull(message = "SubcountyID must be provided")
    public Integer subcountyId;

    public Integer getSubcountyId() {
        return subcountyId;
    }

    public void setSubcountyId(Integer subcountyId) {
        this.subcountyId = subcountyId;
    }
}