package com.airqualitymonitoring.FinalYearProject.requests.subcounty;

import javax.validation.constraints.NotNull;

public class GetSubcountieCountyRequest {
    
    @NotNull(message = "countyID must be provided")
    public Integer countyId;

    private int page;

    private int items;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }
}