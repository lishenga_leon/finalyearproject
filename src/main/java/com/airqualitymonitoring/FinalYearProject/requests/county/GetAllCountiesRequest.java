package com.airqualitymonitoring.FinalYearProject.requests.county;

public class GetAllCountiesRequest {

    public int page;

    public int items;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getItems() {
        return items;
    }

    public void setItems(int items) {
        this.items = items;
    }
}