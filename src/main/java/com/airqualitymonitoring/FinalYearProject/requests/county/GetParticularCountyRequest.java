package com.airqualitymonitoring.FinalYearProject.requests.county;

import javax.validation.constraints.NotNull;

public class GetParticularCountyRequest {

    @NotNull(message = "CountyID must be provided")
    public Integer countyId;

    public Integer getCountyId() {
        return countyId;
    }

    public void setCountyId(Integer countyId) {
        this.countyId = countyId;
    }
}