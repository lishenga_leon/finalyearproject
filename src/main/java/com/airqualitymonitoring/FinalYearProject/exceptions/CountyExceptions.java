package com.airqualitymonitoring.FinalYearProject.exceptions;

@SuppressWarnings("serial")
public class CountyExceptions extends RuntimeException  {

    private Integer code;

	private String message;

	public CountyExceptions(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}