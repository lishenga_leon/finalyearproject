package com.airqualitymonitoring.FinalYearProject.exceptions;

@SuppressWarnings("serial")
public class DeviceExceptions extends RuntimeException  {

    private Integer code;

	private String message;

	public DeviceExceptions(Integer code, String message) {
		super();
		this.code = code;
		this.message = message;
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
}